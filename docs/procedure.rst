.. highlight:: bash

Fiche procédure - Installation de la plateforme
==================================================

A1. OS
------

Le système d'exploitation hôte de la plateforme est Debian 9 x86_64. L'installation de ce dernier est recommandé afin de garder une compatibilité avec les logiciels et la configuration de cette documentation.

Installation
^^^^^^^^^^^^

Lors de l'installation il sera nécessaire de créer une session *keep* et de définir le nom de domaine du serveur à *pst.esiea.fr*.

Lorsque l'assistant demandera quels logiciels installer, choisir : 

 - serveur SSH ;

 - utilitaires usuels du système.

Le reste de l'installation s'effectue de manière classique et le choix des options n'influera pas sur l'installation de la plateforme.

Une fois que la distribution est installée, vérifiez votre configuration réseau à l'aide de la suite de commandes `ip`. Lorsque le réseau fonctionne, il faut commencer par synchroniser les dépôts, faire les mises à jour et installer quelques paquets basiques nécessaires à la plateforme : 
::
su # Entrez votre mot de passe root
	apt update
	apt upgrade
	apt install unzip


Quelques paquets sont requis pour le fonctionnement de la plateforme.

Installation de `sudo`.
Le paquet `sudo` permettra de gérer plus finement les droits des utilisateurs.
Tout d'abord, installez ce paquet : 
::
apt install sudo

Par défaut, cet outil autorise les utilisateurs du groupe *sudo* à pouvoir lancer des commandes en tant que superutilisateur. Afin de pouvoir profiter de cela, ajoutez le groupe *sudo* à l'utilisateur *keep* : 
::
	usermod -a -G sudo keep

Afin que les modifications soient prises en compte, il est nécessaire de se déconnecter puis se reconnecter en tant qu'utilisateur *keep*.

A2. DNS
-------

L'installation d'un serveur DNS sur le serveur est primordial afin de gérer l'accès aux différents conteneurs via l'utilisation de noms de domaine.

Installation
^^^^^^^^^^^^

Le paquet nécessaire peut être installé via la commande suivante : ::
	sudo apt install dnsmasq

	\begin
	```shell=
	# Création du répertoire
	sudo mkdir -p /usr/share/keep/dns

	# Création du fichier de configuration
	sudo touch /usr/share/keep/dns/containers.conf

	# Donne les droits à l'utilisateur keep
	sudo chown -R keep:keep /usr/share/keep
	```

	Redémarrez le serveur DNS et activez le lancement au démarrage :
	```shell=
	# Redémarrage du service
	sudo systemctl restart dnsmasq

	# Activation au démarrage
	sudo systemctl enable dnsmasq

#### Sécurité

#### Tests

Afin de tester le service il est possible d'ajouter quelques lignes dans le fichier */usr/share/keep/dns/containers.conf*.
Voici un exemple qui être utilisé pour réaliser le test : 
```shell=
1.2.3.4 test.pst.esiea.fr
5.6.7.8 test.local
```

La prise en compte de cette configuration nécessite de notifier le serveur dnsmasq du changement. Il est possible d'effectuer cela via la commande suivante : 
```shell=
sudo systemctl reload dnsmasq
```

Pour tester le service dns il est nécessaire d'effectuer des requêtes à l'aide d'un utilitaire tel que `dig`. Si celui-ci n'est pas installé, il sera nécéssaire d'installer le paquet `dnsutils`.
Vous pouvez désormais lancer les commandes suivantes afin de s'assurer du fonctionnement du service : 
```shell=
dig test.pst.esiea.fr @127.0.0.1
dig test.local @127.0.0.1
```

Si tout fonctionne vous devriez retrouver les adresses ip insérées précédemment. N'oubliez pas de retirer les lignes de test du fichier de configuration.

### A3. Service Web

Le service web est la porte d'entrée de notre plateforme. Il permettra d'accéder au site principal et aux différents conteneurs. Le serveur qui sera utilisé est Nginx.

#### Installation (Nginx / Plugins PHP)

Voici les paquets qui doivent être installés : 
```shell=
sudo apt install nginx php7.0 php7.0-fpm php7.0-mysql php7.0-ldap php7.0-intl php7.0-xml composer
```

Configuration de PHP.
Certains modules doivent être ajoutés dans la configuration de PHP. Éditez le fichier */etc/php/7.0/fpm/php.ini* avec les droits superutilisateur.

Dans la section *Dynamic Extensions*, ajoutez les lignes suivantes : 
```shell=
extension=php_ldap.so
extension=php_pdo_mysql.so
```

Modifiez le fichier */etc/nginx/nginx.conf* afin de remplacer la ligne `user www-data` par `user keep`.
De même dans le fichier */etc/php/7.0/fpm/pool.d/www.conf*, remplacez *www-data* par *keep* pour les lignes suivantes : 
```
user = www-data
group = www-data
...
listen.owner = www-data
listen.group = www-data
```

Supprimez les fichiers de configuration par défaut : 
```shell=
sudo rm /etc/nginx/sites-enabled/default
sudo rm /etc/nginx/sites-available/default
sudo rm -r /var/www/html
```

Copiez la configuration du serveur web dans le fichier */etc/nginx/sites-available/keep* puis faites des liens symboliques afin d'activer la configuration : 
```shell=
# Activation de la configuration du serveur web
sudo ln -s /etc/nginx/sites-available/keep /etc/nginx/sites-enabled/keep

# Lien symbolique vers le répertoire www
sudo ln -s /home/keep/www /var/www/keep
```

Pour ne pas avoir d'erreurs lors du lancement de nginx, créez les répertoires de log : 
```shell=
sudo mkdir -p /var/log/nginx/keep/{containers,dev,unwanted}
```

Redémarrez enfin les services nginx et php-fpm pour vérifier qu'il n'y a pas d'erreurs de configuration : 
```shell=
sudo systemctl restart php7.0-fpm nginx
```

#### Sécurité

Il peut être intéressant de cacher certaines informations relatives au serveur web afin d'empêcher certaines personnes malveillantes d'y avoir accès.

Pour cela, installez le paquet **nginx-extras** : 
```shell=
sudo apt install nginx-extras
```

Puis ajoutez les lignes suivantes dans le fichier */etc/nginx/nginx.conf* au début du bloc *http* puis redémarrez à nouveau nginx : 
```shell=
server_tokens off; # Cache la version de Nginx
more_clear_headers Server; # Cache le nom du serveur web
```

Dans un deuxième temps nous allons installer **fail2ban**, un logiciel analysant différents fichiers de log pour détecter des comportants suspects et bannir les clients trop instants.

```shell=
sudo apt install fail2ban
```

Les fichiers de configuration de fail2ban se trouvent dans le répertoire */etc/fail2ban/*.
Il est possible de choisir quels services nous souhaitons monitorer via le fichier *jail.conf*.
Dans le cas de notre plateforme, nous souhaitons bloquer toutes les personnes faisant des requêtes anormales.
La configuration Nginx qui a été réalisée demande au serveur web de placer toutes les requêtes suspectes dans les fichiers */var/log/nginx/keep/unwanted/\*.log*.
Nous allons donc rajouter une règle à la fin du fichier *jail.conf* : 
```shell=
[nginx-keep-limit]
enabled = true
filter = nginx-keep-limit
port = http,https
logpath = /var/log/nginx/keep/unwanted/*.log
findtime = 600
bantime = 7200
maxretry = 5
```

Suite à cela nous devrons définir un filtre lié à cette configuration. Créez le fichier */etc/fail2ban/filter.d/nginx-keep-limit.conf* et placez-y le contenu suivant : 
```shell=
# Fail2Ban configuration file
#

[Definition]

failregex = <HOST> .+

# Option: ignoreregex
# Notes.: regex to ignore. If this regex matches, the line is ignored.
# Values: TEXT
#
ignoreregex =
```

Vous pouvez désormais redémarrer fail2ban pour que les modifications soient effectives : 
```shell=
sudo systemctl restart fail2ban
```

Activez de la même manière le démarrage automatique du service : 
```shell=
sudo systemctl enable fail2ban
```

#### Tests

### A4. LXC

**LXC** est la suite logicielle permettant de créer des conteneurs.

#### Installation

Commencez tout d'abord par installer le paquet `lxc`: 
```shell=
sudo apt install lxc
```

Dans un second temps, vous devrez définir une configuration par défaut de manière à pemettre la création de conteneurs non privilégiés.
Creez le répertoire suivant : 
```shell=
mkdir -p ~/.config/lxc
```
Voici la configuration à mettre en place dans le fichier *~/.config/lxc/default.conf* : 
```=
lxc.id_map = u 0 100000 65536
lxc.id_map = g 0 100000 65536
lxc.mount.auto = proc:mixed sys:ro cgroup:mixed

lxc.network.type = veth
lxc.network.link = lxcbr0
lxc.network.flags = up
```

Ces deux premières lignes doivent correspondre aux deux dernières valeurs retournées par ces deux commandes : 
```shell=
# Ligne 1
cat /etc/subuid | grep $USER

# Ligne 2
cat /etc/subgid | grep $USER
```

Autorisez la création de conteneurs en mode non privilégiés : 
```shell=
sudo sh -c 'echo "kernel.unprivileged_userns_clone=1" > /etc/sysctl.d/80-lxc-userns.conf'
sudo sysctl kernel.unprivileged_userns_clone=1
```

Le réseau interne de lxc doit aussi être configuré en définissant les valeurs suivantes dans le fichier */etc/default/lxc-net* : 
```shell=
USE_LXC_BRIDGE="true"
LXC_BRIDGE="lxcbr0"
LXC_ADDR="10.3.0.1"
LXC_NETMASK="255.255.0.0"
LXC_NETWORK="10.3.0.0/16"
```

Puis enfin, le fichier */etc/lxc/lxc-usernet* permettra à un utilisateur d'utiliser le bridge lxc *lxcbr0* : 
```shell=
keep veth lxcbr0 65000
```
Cette ligne autorise l'utilisateur *keep* à utiliser le bridge *lxcbr0* avec 65 000 conteneurs.

Maintenant que LXC et le Dnsmasq sont installés, il faut savoir que ces deux-ci rentrent en conflit lors du redémarrage du serveur. En effet, nous utilisons Dnsmasq pour la gestion des noms de domaine mais LXC l'utilise aussi pour l'allocation d'adresses IP pour ses conteneurs. Cependant, nous n'avons pas besoin de cette fonctionnalité et cette dernière n'est pas désactivable simplement. Nous allons donc créer un service avec **systemd**. Créez un fichier dans */lib/systemd/system/dnsmasqlxc.service* et placez-y le contenu suivant : 
```shell=
[Unit]
Description=Kill Dnsmasq started by lxc-net
After=lxc-net.service
Before=dnsmasq.service

[Service]
Type=oneshot
ExecStart=/usr/bin/killall -9 dnsmasq
ExecStop=/bin/true

[Install]
WantedBy=multi-user.target
```

Puis activez le service via la commande ci-dessous.
```shell=
sudo systemctl enable dnsmasqlxc

sudo systemctl stop dnsmasq
sudo systemctl stop lxc-net
sudo systemctl start lxc-net
sudo systemctl start dnsmasqlxc
sudo systemctl start dnsmasq

```
:::warning
Placer les scripts dans /usr/share/keep/scripts
```shell=
sudo mkdir -p /usr/share/keep/scripts
```:::

#### Sécurité

#### Tests

### B1. MariaDB

**MariaDB** est le système de gestion de bases de données de notre plateforme. Pour plus de sécurité, ce dernier est installé au sein d'un conteneur.

#### Installation

Création d'un conteneur.:::warning
Le conteneur de type Debian/Ubuntu pose quelques problèmes : problèmes de PATH, locales.
Debian : manque les logiciels de base : ping, vim, vi, nano, ...
```shell=
lxc-create -n database -t download -- --dist debian --release stretch --arch amd64
```:::

```shell=
lxc-create -n database -t download -- --dist ubuntu --release xenial --arch amd64
```

Installation du système de gestion de bases de données.:::warning
Lors du retest général, vérifier si le conteneur démarre bien. Si ce n'est pas le cas, il faudra inviter l'utilisateur à se déconnecter/connecter.:::
```shell=
# Démarrage du conteneur
lxc-start -n database

# Configuration du réseau
export CT_IP=10.3.0.2;
lxc-attach -n database < /usr/share/keep/scripts/utils/setNetwork.sh;
unset CT_IP;

# Génération des locales
lxc-attach -n database /usr/sbin/locale-gen fr_FR.UTF-8

# Accès au conteneur
lxc-attach -n database su
```

À partir de là, vous devriez être en *root* dans le conteneur.
L'installation de **MariaDB** peut alors être réalisée.
```shell=
# Installation de MariaDB
apt install mariadb-server
```

Configurez la base de données : 
```shell=
mysql_secure_installation
```
Définissez un mot de passe pour le compte *root* et répondez positivement aux questions posées.

Éditez ensuite le fichier */etc/mysql/mariadb.conf.d/50-server.cnf* afin de changer l'adresse d'écoute du serveur mysql pour pouvoir y accéder depuis le système hôte : 
```shell=
bind-address = 10.3.0.2
```
Il faudra ici remplacer l'adresse par défaut qui est 127.0.0.1 par 10.3.0.2.

Redémarrez ensuite le serveur mysql afin que les modifications soient prises en compte : 
```shell=
systemctl restart mysql
```

Pour la suite de l'installation, deux comptes supplémentaires doivent être créés pour le site principal de la plateforme : 
```shell=
# Entrez le mot de passe renseigné précédemment
mysql -u root -p
```
Dans l'invité de commande *mysql*, entrez ceci en prenant soin de changer les mots de passe par défaut : 
```sql=
-- Création de la base de données keep
CREATE DATABASE keep;

-- Création d'un compte pour le site principal
CREATE USER 'keep'@'10.3.0.1' IDENTIFIED BY 'motDePasseSécurisé';
GRANT ALL ON keep.* TO 'keep'@'10.3.0.1';

-- Création d'un compte pour la gestion interne
CREATE USER 'manager'@'10.3.0.1' IDENTIFIED BY 'deuxiemeMotDePasseSécurisé';
GRANT ALL ON *.* TO 'manager'@'10.3.0.1';

-- On sort de l'invité de commande
exit;
```

À la suite de cela, nous allons configurer la connexion automatique à la base de données de façon à simplifier son accès via les différents scripts.
Pour cela, commencez par sortir du conteneur afin de retourner sur la session keep.
Installez le paquet `mysql-client` : 
```shell=
sudo apt install mysql-client
```
Creez ensuite un fichier *~/.my.cnf* dont le contenu est le suivant : 
```
[client]
user=manager
password=motDePasseDuCompteManager
host=10.3.0.2
```

Pour terminer, passons le conteneur *database* en démarrage automatique : 
```shell=
echo "lxc.start.auto = 1" >> ~/.local/share/lxc/database/config
```

La base de données est désormais configurée.

#### Sécurité

#### Tests

Vous pouvez tester la connexion automatique à la base de données. Vous devriez vous retrouver connecté sans saisie de mot de passe au SGBD si vous lancez la commande suivante : 
```shell=
mysql
```

### C. Nest

**Nest** est le conteneur modèle de notre infrastructure. Ce conteneur a pour but de contenir les logiciels qui seront proposés aux utilisateurs. Ce conteneur est copié lorsqu'un projet est créé.

#### Installation

La création de ce conteneur **Nest** est possible en lançant le script suivant : 
```shell=
/usr/share/keep/scripts/nest.sh
```

Ce script a pour but d'installer l'ensemble des modules présentés qui ont été retenus dans ce rapport.
Le conteneur résultant n'est pas directement utilisable. En effet, les accès à la base de données ne sont pas encore configurés. Cela est réalisé quand un projet est créé de façon à bien différentier les données des projets.

#### Sécurité

#### Tests

### D. Site principal

Le site principal est la façade de la plateforme. Elle permettra aux utilisateurs d'accéder à leurs conteneurs, aux administrateurs de gérer la plateforme et aux visiteurs de rechercher des projets.

#### Installation

Placez vous dans le répertoire de la session *keep*, créez un répertoire *www* puis copiez le code relatif au site principal à l'intérieur de ce dernier : 
```shell=
mkdir ~/www
cd ~/www
```

Une fois le code source du site en place, il sera nécessaire d'installer la base du framework **Symfony**.
```shell=
composer install
```

Durant ce processus, vous serez invité à entrer les éléments de configuration dont le site a besoin tels que les informations de connexion à la base de données ou encore l'adresse du serveur LDAP.

Voici à quoi correspondent les différentes options qui seront demandées : 
- **database_host** : Adresse vers le serveur de gestion de base de données. Dans le cas de notre plateforme, nous mettrons **10.3.0.2** ;
- **database_port** : Port du serveur de gestion de base de données. Nous utiliserons le port par défaut qui est **3306** pour MariaDB ;
- **database_name** : Le nom de la base de données correponsant au site principal. Ici nous la nommerons **keep** ;
- **database_user** : Nom de l'utilisateur ayant les droits de lecture et écriture dans la base de données définie ci-dessus. Nous mettrons **keep**, le compte SQL créé précédemment. ;
- **database_password** : Mot de passe associé à l'utilisateur **database_user** ;
- **mailer_\*** : Ces options ne sont pas utilisées. Les valeurs par défaut conviendront ;
- **secret** : Valeur aléatoire permettant d'améliorer la sécurité du framework ;
- **app.domain** : Nom de domaine de la plateforme précédé par un point. Dans le cas de l'ESIEA, le nom de domaine choisi est **.pst.esiea.fr** ;
- **app.ldap_host** : Adresse du serveur LDAP ;
- **app.ldap_port** : Port du serveur LDAP. Par défaut il s'agit de **389** ;
- **app.ldap_user** : Chemin vers l'utilisateur ayant les droits de lecture dans l'annuaire ;
- **app.ldap_password** : Mot de passe de l'utilisateur **app.ldap_user** ;
- **app.ldap_basedn** : Base de l'annuaire à partir de laquelle la recherche d'utilisateur est faite ;
- **app.ldap_default_roles** : Roles par défaut qu'on les utilisateurs lors de leur inscription sur le site. Nous mettrons **ROLE_USER** dans notre cas ;
- **app.ldap_uid_key** : Nom de la clé associé à un utilisateur dans l'annuaire. La valeur par défaut est **cn** ;
- **app.ldap_filter** : Filtre utilisé pour la recherche d'utilisateurs. La valeur par défaut sera utilisée :  **({uid_key}={username})** ;
- **app.ldap_password_attribute** : Cette valeur n'est pas utilisée et sera mise à sa valeur par défaut.

Vous devrez par la suite envoyer la structure de la base de données keep vers le SGBD : 
```shell=
bin/console doctrine:schema:update --force